package com.example.jonathanvazquez.podometerwithsteeps;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.jonathanvazquez.SensorAcelerometer.SteepDetector;
import com.example.jonathanvazquez.SensorAcelerometer.StepListener;
import com.example.jonathanvazquez.podometerwithsteeps.R;

public class MainActivity extends AppCompatActivity implements SensorEventListener, StepListener, View.OnClickListener {

    public Button buttonStart;
    public Button buttonStop;
    public TextView txvSteeps;

    private SteepDetector simpleSteep;
    private int numSteps;
    private SensorManager sensorManager;
    private Sensor accel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleSteep = new SteepDetector();
        simpleSteep.registerListener(this);

        txvSteeps = findViewById(R.id.txvSteeps);

        buttonStart = findViewById(R.id.buttonStart);
        buttonStart.setOnClickListener(this);

        buttonStop = findViewById(R.id.buttonStop);
        buttonStop.setOnClickListener(this);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleSteep.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void step(long timeNs) {
        numSteps++;
        txvSteeps.setText(String.valueOf(numSteps));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonStart:
                sensorManager.registerListener(this, accel, SensorManager.SENSOR_DELAY_NORMAL);
                break;
            case R.id.buttonStop:
                sensorManager.unregisterListener(this);
                break;
        }
    }
}
