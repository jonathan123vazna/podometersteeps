package com.example.jonathanvazquez.SensorAcelerometer;

public interface StepListener {
    public void step(long timeNs);
}
