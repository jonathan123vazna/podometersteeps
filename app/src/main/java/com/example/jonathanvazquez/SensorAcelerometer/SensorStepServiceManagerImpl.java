package com.example.jonathanvazquez.SensorAcelerometer;

import com.orangegangsters.github.lib.SensorStepServiceManager;

public class SensorStepServiceManagerImpl extends SensorStepServiceManager {

    @Override
    public Class getReceiverClass() {
        return SensorStepReceiverImpl.class;
    }
}
